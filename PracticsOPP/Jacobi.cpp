#include "Jacobi.h"
#include <iostream>

void grid_print(Grid grid)
{
    for (int i = 1; i < grid.xlength()+1; i++) {
        for (int j = 0; j < grid.ylength(); j++)
        {
            double var = grid(i, j);
            std::cout << var;
        }
        std::cout << "\n";
    }
    std::cout << "\n";
}

bool is_boundary_cell(int index, int bound)
{
    return index == 0 || index == bound - 1;
}


void grid_init(Grid grid, Grid previous_grid)
{
    for (int i = 1; i < grid.xlength() + 1; i++)
    {
        for (int j = 0; j < grid.ylength(); j++)
        {
            grid(i, j) = PHI0;
            previous_grid(i, j) = PHI0;

            int actual_i = i + grid.shift();

            if (is_boundary_cell(actual_i, NX) || is_boundary_cell(j, NY))
            {
                grid(i, j) = phi(actual_i, j);
                previous_grid(i, j) = phi(actual_i, j);
            }
        }
    }
}

void update_grid_center(Grid grid, Grid previous_grid, double *delta)
{
    int center = (grid.xlength() + 1) / 2;

    update_grid_cell(center, grid, previous_grid, delta);

    for (int j = 1; j < center; j++)
    {
        update_grid_cell(center - j, grid, previous_grid, delta);
        update_grid_cell(center + j, grid, previous_grid, delta);
    }

    if (grid.xlength() % 2 == 0)
    {
        update_grid_cell(grid.xlength(), grid, previous_grid, delta);
    }
}

void update_grid_bound(Grid grid, Grid previous_grid, double * delta)
{
    update_grid_cell(1, grid, previous_grid, delta);
    update_grid_cell(grid.xlength(), grid, previous_grid, delta);
}

double iteration_func(int i, int j, Grid grid)
{
    return ((1.0 / (2.0 / (hx * hx) + 2.0 / (hy * hy) + A)) *
            ((grid(i + 1, j) + grid(i - 1, j)) / (hx * hx) +
             (grid(i, j + 1) + grid(i, j - 1)) / (hy * hy) -
             ro(i + grid.shift(), j)));
}

void update_grid_cell(int index, Grid grid, Grid previous_grid, double *delta)
{
    int actual_index = index + grid.shift(); 

    if (!is_boundary_cell(actual_index, NX))
    {
        for (int j = 1; j < grid.ylength() - 1; j++)
        {
            grid(index, j) = iteration_func(index, j, previous_grid);

            double cur_delta = fabs(grid(index, j) - previous_grid(index, j));

            if (*delta < cur_delta)
            {
                *delta = cur_delta;
            }
        }
    }

    for (int j = 1; j < grid.ylength() - 1; j++)
    {
        previous_grid(index, j) = grid(index, j);
    }
}

int jacobi_method(Grid grid, Grid previous_grid)
{
    grid_init(grid, previous_grid);

    int iteration_count = 0;
    double max_delta = 0;

    do
    {
        double delta = 0;

        previous_grid.send_grid_bound();
        update_grid_center(grid, previous_grid, &delta);
        previous_grid.wait_grid_bound();
        update_grid_bound(grid, previous_grid, &delta);
        ParaCommunication::updateAllMax(&delta, &max_delta);
        iteration_count++;

    } while (max_delta >= E);

    return iteration_count;
}

double get_observational_error(Grid grid)
{
    double pr_observational_error = 0;

    for (int i = 1; i < grid.xlength() + 1; i++)
    {
        for (int j = 0; j < NY; j++)
        {
            int actual_i = i + grid.shift();
            double temp = fabs(grid(i, j) - phi(actual_i, j));
            if (temp > pr_observational_error)
            {
                pr_observational_error = temp;
            }
        }
    }

    double observational_error = 0;
    ParaCommunication::updateMax(&pr_observational_error, &observational_error);
    return observational_error;
}

#ifndef MPI_JACOBI_JACOBI_H
#define MPI_JACOBI_JACOBI_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "./CommonsParaLib/CommonsParaLib.h"
#include "baseline_data.h"

int jacobi_method(Grid grid, Grid previous_grid);

void update_grid_cell(int index, Grid grid, Grid previous_grid, double* delta);

void update_grid_center(Grid grid, Grid previous_grid, double* delta);

void update_grid_bound(Grid grid, Grid previous_grid, double* delta);

void grid_init(Grid grid, Grid previous_grid);

double iteration_func(int i, int j, Grid grid);

void grid_print(Grid grid);

double get_observational_error(Grid grid);

#endif //MPI_JACOBI_JACOBI_H

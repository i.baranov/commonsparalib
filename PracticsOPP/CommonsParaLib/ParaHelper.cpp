#include "CommonsParaLib.h"

ParaHelper::ParaHelper(int argc, char** argv)
	{
		MPI_Init(&argc, &argv);
	}

void ParaHelper::getNumberOfProcceses(int * comm_size){
		MPI_Comm_size(MPI_COMM_WORLD, comm_size);
	}

void ParaHelper::getCurrentProccesNumber(int* rank) {
		MPI_Comm_rank(MPI_COMM_WORLD, rank);
	}

ParaHelper::~ParaHelper()
	{
		MPI_Finalize();
	}
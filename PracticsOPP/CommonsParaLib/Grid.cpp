#include "CommonsParaLib.h"
#include <iostream>

Grid::Grid(int commsize, int rank, int xSize, int ySize) {
	this->commsize = commsize;
	this->rank = rank;
	this->xSize = xSize/commsize;
	this->ySize = ySize;
	this->request_prev = new MPI_Request[2];
	this->request_next = new MPI_Request[2];

	this->gridArray = new double[(xSize / commsize + BORDERS_COUNT) * ySize];
}

Grid::~Grid()
{
	//delete gridArray;
}

int Grid::length()
{
	return (xSize + BORDERS_COUNT) * ySize;
}

int Grid::xlength()
{
	return this->xSize;
}

int Grid::ylength()
{
	return this->ySize;
}

int Grid::shift()
{
	return rank * commsize - 1;
}

void Grid::send_grid_bound()
{
	int layer_size = this->ySize;
	if (this->rank != 0)
	{
		MPI_Isend(gridArray + layer_size, layer_size, MPI_DOUBLE, this->rank - 1, 0, MPI_COMM_WORLD, &request_next[0]);
		MPI_Irecv(gridArray, layer_size, MPI_DOUBLE, this->rank - 1, 0, MPI_COMM_WORLD, &request_next[1]);
	}
	if (this->rank != this->commsize - 1)
	{
		MPI_Isend(gridArray + (this->xSize)*layer_size, layer_size, MPI_DOUBLE, this->rank + 1, 0, MPI_COMM_WORLD,
			&request_prev[0]);
		MPI_Irecv(gridArray + (this->xSize + 1) * layer_size, layer_size, MPI_DOUBLE, this->rank + 1, 0, MPI_COMM_WORLD,
			&request_prev[1]);
	}
}

void Grid::wait_grid_bound()
{
	if (this->rank != 0)
	{
		MPI_Wait(&request_next[0], MPI_STATUS_IGNORE);
		MPI_Wait(&request_next[1], MPI_STATUS_IGNORE);
	}
	if (this->rank != this->commsize - 1)
	{
		MPI_Wait(&request_prev[0], MPI_STATUS_IGNORE);
		MPI_Wait(&request_prev[1], MPI_STATUS_IGNORE);
	}
}

double& Grid::operator()(int i, int j)
{
	return this->gridArray[i*this->ySize + j];
}

const double Grid::operator()(int i, int j) const
{
	return this->gridArray[i * this->ySize + j];
};
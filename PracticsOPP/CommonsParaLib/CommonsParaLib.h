#pragma once
#include <mpi.h>
#define BORDERS_COUNT 2

class Grid {
public:
	Grid(int commsize, int rank, int xSize, int ySize);
	~Grid();
	double& operator()(int i, int j);
	const double operator()(int i, int j) const;
	int length();
	int xlength();
	int ylength();
	int shift();
	void send_grid_bound();
	void wait_grid_bound();
private:
	int rank;
	int commsize;
	int xSize;
	int ySize;
	double* gridArray;
	MPI_Request * request_prev;
	MPI_Request * request_next;
};

class ParaHelper {
public:
	ParaHelper(int argc, char** argv);
	~ParaHelper();
	void getNumberOfProcceses(int* comm_size);
	void getCurrentProccesNumber(int* rank);
};

static class ParaCommunication {
public:
	static void updateAllMax(double* a, double* b);
	static void updateMax(double* a, double* b);
};


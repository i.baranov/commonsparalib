#include "baseline_data.h"
#include <iostream>
#include <mpi.h>

double phi(double i, double j)
{
    double x = X0 + (i) * hx;
    double y = X0 + (j) * hy; 
    return x * x + y * y;
}

double ro(double i, double j)
{
    return 6 - A * phi(i, j);
}

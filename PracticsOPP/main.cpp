#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include "Jacobi.h"
#include "./CommonsParaLib/CommonsParaLib.h"

int main(int argc, char **argv)
{
    int pr_rank, comm_size;
    ParaHelper parahelper(argc, argv);
    parahelper.getNumberOfProcceses(&comm_size);
    parahelper.getCurrentProccesNumber(&pr_rank);

    if (NX % comm_size != 0)
    {
        std::cerr << ("Grid size should be multiple of number of processes\n");
        return 0;
    }

    Grid grid(comm_size, pr_rank, NX, NY);

    Grid prev_grid(comm_size, pr_rank, NX, NY);
    
    double t_start = MPI_Wtime();
    int iteration_count = jacobi_method(grid, prev_grid);
    double t_end = MPI_Wtime();
    
    double delta = get_observational_error(grid);

    if (pr_rank == 0)
    {
        printf("Iteration count: %d\n", iteration_count);
        printf("Time: %f\n", t_end - t_start);
        printf("Delta: %lf\n", delta);
    }

    return 0;
}